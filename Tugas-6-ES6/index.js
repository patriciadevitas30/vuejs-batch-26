// soal 1
const luas = (panjang,lebar) => {
    return panjang*lebar;
}

const keliling = (panjang,lebar) => {
    return (2*panjang) + (2*lebar);
}

let lebar = 4;
let panjang = 9;
console.log(luas(panjang,lebar));
console.log(keliling(panjang,lebar));

//soal 2
const newFunction = (firstName, lastName) =>{
    return {
      fullName(){
        console.log(firstName + " " + lastName)
      }
    }
}
newFunction("William", "Imoh").fullName();

//soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const {firstName, lastName, address, hobby} = newObject;
console.log(firstName, lastName, address, hobby);

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east];
console.log(combined);

//soal 5
const planet = "earth" 
const view = "glass" 
const after = `Lorem ${view} dolor sit amet,consectetur adipiscing elit, ${planet} `;
console.log(after);

