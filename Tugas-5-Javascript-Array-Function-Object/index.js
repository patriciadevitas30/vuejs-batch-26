//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var daftarHewansort = daftarHewan.sort();
for (var i=0;i<5;i++){
    console.log(daftarHewansort[i]);
}

//soal 2
function introduce(data){
    var olahdata = "Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby; 
    return olahdata;
}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" };
var perkenalan = introduce(data);
console.log(perkenalan);

//soal 3
function hitung_huruf_vokal (nama){
    var jmlhkarakter = nama.length;
    var count=0;
    for (var i=0;i<jmlhkarakter;i++){
        if(nama[i] == "a" ||nama[i] == "A"){
            count++;
        }
        else if(nama[i] == "i" || nama[i] == "I"){
            count++;
        }
        else if(nama[i] == "u" || nama[i] == "U"){
            count++;
        }
        else if(nama[i] == "e" || nama[i] == "E"){
            count++;
        }
        else if(nama[i] == "o" || nama[i] == "O"){
            count++;
        }
    }
    return count;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 ,hitung_2);

//soal 4
function hitung(value){
    var rumus = (2*value)-2;
    return rumus;
}
console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));
