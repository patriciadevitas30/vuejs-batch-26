//soal 1
var nilai = 88;
if(nilai >= 85){
    console.log("A");
} 
else if (nilai >= 75 || nilai < 85){
    console.log("B");
} 
else if (nilai >= 65 || nilai < 75){
    console.log("C");
} 
else if (nilai >= 55 || nilai < 65){
    console.log("D");
} 
else if (nilai < 55){
    console.log("E");
}

//soal 2
var tanggal = 17;
var bulan = 3;
var tahun = 2001;

var strtanggal = String (tanggal);
var strtahun = String (tahun);

switch(bulan){
    case 1 :{
        console.log(strtanggal + " Januari " + strtahun);
        break;
    }

    case 2:{
        console.log(strtanggal + " Februari " + strtahun);
        break;
    }

    case 3:{
        console.log(strtanggal + " Maret " + strtahun);
        break;
    }

    case 4:{
        console.log(strtanggal + " April " + strtahun);
        break;
    }

    case 5:{
        console.log(strtanggal + " Mei " + strtahun);
        break;
    }

    case 6:{
        console.log(strtanggal + " Juni " + strtahun);
        break;
    }

    case 7:{
        console.log(strtanggal + " Juli " + strtahun);
        break;
    }

    case 8:{
        console.log(strtanggal + " Agustus " + strtahun);
        break;
    }

    case 9:{
        console.log(strtanggal + " September " + strtahun);
        break;
    }

    case 10:{
        console.log(strtanggal + " Oktober " + strtahun);
        break;
    }

    case 11:{
        console.log(strtanggal + " November " + strtahun);
        break;
    }

    case 12:{
        console.log(strtanggal + " Desember " + strtahun);
        break;
    }

}

//soal 3
var value = 4;
var temp = '';
for (var i = 0; i < value; i++){
    for (var j = 0; j <= i; j++){
        temp += '* ';
    }
    temp += '\n';
}
console.log(temp);

//soal 4
var m = 11;
var count=1;
var loop=0;
var tamp = '';
for (var i=1;i<=m;i++){
    if(count==1)
    {
        console.log(i + " - " + "I love Programming" + "\n");
        count++;
    }
    else if(count==2)
    {
        console.log(i + " - " + "I love Javascript" + "\n");
        count++;
    }
    else if(count==3)
    {
        console.log(i + " - " + "I love VueJS" + "\n");
        loop++;
        for(var j=0;j<loop;j++)
        {
            tamp += '===';
        }
        console.log(tamp);
        tamp = '';
        count=1;
    }
}

