//soal 1 : Function Penghitung Jumlah Kata
function jumlah_kata(kalimat){
    var jmlhkarakter = kalimat.length;
    var count=0;
    for(var i=0;i<jmlhkarakter;i++){
        if(kalimat[i]==" " && i!=0 && i!=jmlhkarakter-1){
            count++;
        }
    }
    return count+1;
}
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = "Saya Iqbal";

console.log(jumlah_kata(kalimat_1));
console.log(jumlah_kata(kalimat_2));

//soal 2 : Function Penghasil Tanggal Hari Esok
function next_date(tanggal,bulan,tahun){
    if(tanggal==31){
        if(bulan==12){
            tanggal=1;
            bulan=1;
            tahun++;
        }
        else{
            tanggal=1;
            bulan++;
        }
    }
    else if(bulan==2){
        if(tanggal==29 && tahun%4==0 || tanggal==28 && tahun%4>0){
            tanggal=1;
            bulan++;
        }
    }
    else{
        tanggal++;
    }
    
    var strtanggal = String (tanggal);
    var strtahun = String (tahun);

    switch(bulan){
        case 1 :{
            return strtanggal + " Januari " + strtahun;
            break;
        }
    
        case 2:{
            return strtanggal + " Februari " + strtahun;
            break;
        }
    
        case 3:{
            return strtanggal + " Maret " + strtahun;
            break;
        }
    
        case 4:{
            return strtanggal + " April " + strtahun;
            break;
        }
    
        case 5:{
            return strtanggal + " Mei " + strtahun;
            break;
        }
    
        case 6:{
            return strtanggal + " Juni " + strtahun;
            break;
        }
    
        case 7:{
            return strtanggal + " Juli " + strtahun;
            break;
        }
    
        case 8:{
            return strtanggal + " Agustus " + strtahun;
            break;
        }
    
        case 9:{
            return strtanggal + " September " + strtahun;
            break;
        }
    
        case 10:{
            return strtanggal + " Oktober " + strtahun;
            break;
        }
    
        case 11:{
            return strtanggal + " November " + strtahun;
            break;
        }
    
        case 12:{
            return strtanggal + " Desember " + strtahun;
            break;
        }
    }
}
contoh 1
var tanggal = 29;
var bulan = 2;
var tahun = 2020;

/*contoh 2
var tanggal = 28;
var bulan = 2;
var tahun = 2021;*/

/*contoh 3
var tanggal = 31;
var bulan = 12;
var tahun = 2020;*/

console.log(next_date(tanggal,bulan,tahun));